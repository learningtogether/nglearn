/**
* formExample Module
*
* Description
*/
var app = angular.module('formExample', []);
    
var INTEGER_REGEXP = /^\-?\d+$/;
app.directive('integer', function(){
    // Runs during compile
    return {
        require: 'ngModel', 
        link: function(scope, elm, attrs, ctrl) {
            ctrl.$parsers.unshift(function(viewValue) {
                if(INTEGER_REGEXP.test(viewValue)) {
                    //it is valid
                    ctrl.$setValidity('integer', true);
                    return viewValue;
                } 
                else {
                    //it is invalid
                    ctrl.$setValidity('integer', false);
                    return undefined;
                }
            });
        }
    };
});

var FLOAT_REGEXP = /^\-?\d+((\.|\,)\d+)?$/;
app.directive('smartFloat', function(){
    // Runs during compile    
    return {
        require: 'ngModel',
        link: function(scope, elm, attrs, ctrl) {
            ctrl.$parsers.unshift(function(viewValue) {
                if(FLOAT_REGEXP.test(viewValue)) {
                    //it is valid
                    ctrl.$setValidity('float', true);
                    return parseFloat(viewValue.replace(',', '.'));
                } 
                else {
                    //it is invalid
                    ctrl.$setValidity('float', false);
                    return undefined;
                }
            });
        }
    };
});

app.directive('contenteditable', function(){
    // Runs during compile
    return {
        require: 'ngModel',
        link: function(scope, elm, attrs, ctrl) {
            //view -> model
            elm.on('blur', function () {
                scope.$apply(function() {
                    ctrl.$setViewValue(elm.html());
                });
            });

            // model -> view
            ctrl.$render = function () {
                elm.html(ctrl.$viewValue);
            };

            //load initial value from DOM
            ctrl.$setViewValue(elm.html());
        }
    };
});

app.filter('reverse', function() {
    return function(text) {
        return text.split("").reverse().join("");
    };
});

app.controller('ExampleController', ['$scope', function($scope){
    $scope.master = {};

    $scope.update = function (user) {
        $scope.master = angular.copy(user);
    };

    $scope.reset = function () {
        $scope.user = angular.copy($scope.master);
    };

    $scope.clear = function () {
        $scope.user = {};
    };

    $scope.isUnchanged = function (user) {
        return angular.equals(user, $scope.master);
    };

    $scope.reset();
}]);