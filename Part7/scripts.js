var app = angular.module("app", []);

app.config(function($logProvider) {
    $logProvider.debugEnabled(false);
});

app.run(function($rootScope, $log){
    $rootScope.$log = $log;
});

app.controller('foo', ['$scope', function($scope){
    this.myFunc = function(ev) {
        console.log("myFunc "+ ev.target);
    };
}])

app.directive('dumbPassword', function(){
    var validElement = angular.element("<span> {{ dir.input }}</span>");

    this.link = function(scope) {
        scope.$watch("dir.input", function(value) {
            if (value === "password") {
                validElement.toggleClass("label label-important");
            }
        });
    };

    return {
        restrict: 'E', // E = Element, A = Attribute, C = Class, M = Comment
        templateUrl: 'templates/dumbpass.html',
        replace: true,
        compile: function(tElement) {
            tElement.append(validElement);

            return link;
        }
    };
});
