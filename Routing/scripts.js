var app = angular.module("app", ["ngRoute"]);

app.config(['$routeProvider',function ($routeProvider) {
    $routeProvider

        .when("/", {
            templateUrl: "Templates/app.html",
            controller: "ViewCtrl",
            resolve: {
                loadData: viewCtrl.loadData,
                prepData: viewCtrl.prepData
            }
        })

        .when("/map/:country/:state/:city", {
            templateUrl: "Templates/map.html",
            controller: "MapCtrl"
        })

        .when("/pizza", {
            template: "<div class='alert alert-success'>Pizza is health food</div>"
        })

        .when("/weapon/:weapon", {
            template: "<div class='alert alert-info'></div>",
            controller: "WeaponCtrl"
        })

        .otherwise({
            templateUrl: "Templates/404.html"
        });
}]);

app.directive('error', [ '$rootScope', function ($rootScope){
    return {
        restrict: "E",
        templateUrl: "Templates/error.html",
        scope: {},
        link: function (scope) {
            $rootScope.$on("$routeChangeError", function (event, current, previous, rejection) {
                scope.isError = true;
                scope.errorMsg = "Failed to change routes with message: " + rejection;
            });
        }
    };
}]);

var viewCtrl = app.controller('ViewCtrl', ['$scope', '$route', function ($scope, $route){
    var locals = $route.current.locals;
    $scope.model = {
        message: "View Ctrl says hello!",
        loadMessage: locals.loadData,
        prepMessage: locals.prepData
    };
}]);

viewCtrl.loadData = function ($q, $timeout) {
    var defer = $q.defer();
                    
    $timeout(function () {
        //defer.reject("stuff from loadData");
        defer.resolve("stuff from loadData");
    }, 500);
    
    return defer.promise;
};

viewCtrl.prepData = function ($q, $timeout) {
    var defer = $q.defer();
                    
    $timeout(function () {
        defer.resolve("prepping Data, hold on a sec");
    }, 500);
    
    return defer.promise;
};

app.controller('AppCtrl', ['$rootScope', function ($rootScope){
    $rootScope.$on("$routeChangeError", function (event, current, previous, rejection) {
        //The event is the event,
        //current is the current route,
        //previous is the previous route, which is undefined if the current is the first route
        //and rejection is a custom message that can be passed in from the defer.reject

        console.log("failed to change routes with message:" + rejection);
    });
}]);

app.controller('MapCtrl', ['$scope', '$routeParams', function ($scope, $routeParams){
    $scope.model = {
        country : $routeParams.country,
        state :$routeParams.state,
        city: $routeParams.city
    }
}]);

app.controller('WeaponCtrl', ['$scope', '$q', '$routeParams', function ($scope, $q, $routeParams){
    var defer = $q.defer();

    defer.promise
        .then(function (weapon) {
            console.log("You can have my " + weapon);

            return "axe";
        })
        .then(function (weapon) {
            console.log("And my "+ weapon);

            return "mace";
        })
        .then(function (weapon) {
            console.log("And my " +weapon);
        });

    defer.resolve($routeParams.weapon);
}])