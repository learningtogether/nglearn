var app = angular.module('dTest', []);

//Using Controller As Syntax
//*******************************************************************
app.controller('RoomController', function(){
   this.openDoor = function ()  {
        console.log("Door opens makes creak");
   };
   this.buttonTitle = "Click Me";
});

app.controller('ClosetController', function () {
    this.openDoor = function() {
        console.log("Come out come out, wherever you are");
    };
    this.buttonTitle = "Come on out";
})



//Isolate Scope - @ for strings and = for bi-directional binding
//*******************************************************************
app.controller('DrinkCtrl', ['$scope', function($scope){
    $scope.ctrlFlavor = "blackberry";
}]);

app.directive("twoway", function () {
    return {
        scope: {
            flavor: "="
        },
        template: "<input type='text' ng-model='flavor'> = Bi-directional"
    };
});

app.directive("drink", function () {
    return {
        scope: {
            flavor: "@"
        },
        template: "<input type='text' ng-model='flavor'> @ Uni-directional"
    };
});



//Isolate Scope - Intro. Kids and chores
//*******************************************************************
app.controller('ChoreCtrl', ['$scope', function($scope){
    $scope.logChore = function (chore) {
        console.log(chore + " is done!");
    };
}]);


app.directive("kid", function () {
    return {
        restrict: "E",
        scope: {
            done:"&"
        },
        template: "<div> "+
                    "<input type='text' ng-model='chore'>  "+
                    "<button class='btn btn-inverse' "+
                    "ng-click='done({ chore: chore})'>I'm done</button>{{chore}}"+
                  "</div>"
    };
});


//Heirarchical directive communication
//*******************************************************************

// The country is at the top of the heirarchy
app.directive("country", function(){
    return {
        restrict: "E",
        transclude: true,
        template: "<div class='well'>Country<div ng-transclude></div></div>",
        controller: function() {
            this.makeAnnouncement = function(message) {
                console.log("Country says: "+ message);
            };
        }
    };
});

//The ^ in the require tells angular that the country is above state 
//in the heirarchy
app.directive("state", function() {
    return {
        restrict: "E",
        transclude: true,
        template: "<div class='well well-small'>State<div ng-transclude></div></div>",
        require: "^country",  
        controller: function() {
            this.makeLaw = function(law) {
                console.log("Law "+ law);
            };
        },
        link: function (scope, element, attrs, countryCtrl) {
            //Can talk to country using the countryCtrl here
        }
    };
});

// Multiple requires are possible using an array, which turns the link
// functions controller parameter into an array of controllers
app.directive('city', function(){
    return {
        restrict: 'E',
        template: "<div class='alert alert-info'>City</div>",
        require: ["^country", "^state"],
        link: function(scope, element, attrs, ctrls) {
            ctrls[0].makeAnnouncement("from city");
            ctrls[1].makeLaw("from city says: 'Jump Higher!'");
        }
    };
});


//Components and Containers w/ Transclusion
//*******************************************************************

//The clock is a component that goes into a container
app.directive('clock', function(){
    return {
        restrict: 'E',
        scope: {
            timezone: "@",
            title: "@"
        },
        template: '<div class="alert">12:00pm {{timezone}} {{title}}</div>'        
    };
});

//The panel is a container that contains components, that are allowed
//to be contained by transclusion
app.directive('panel', function(){
    return {
        restrict: 'E',
        scope: {
            title : "@"
        },
        transclude: true,
        template: '<div class="well">{{title}}'+
                        '<div ng-transclude></div>'+
                    '</div>'    
    };
});


//Directive to Directive Communication
//*******************************************************************
app.directive('superhero', function(){
    return {        
        restrict: 'E', // E = Element, A = Attribute, C = Class, M = Comment
        scope: {}, // {} = isolate, true = child, false/undefined = no change        
        controller: function($scope) {
            $scope.abilities = [];

            this.addStrength = function() {
                $scope.abilities.push("strength");
            };

            this.addSpeed = function() {
                $scope.abilities.push("speed");
            };

            this.addFlight = function() {
                $scope.abilities.push("flight");
            };
        },
        link: function(scope, element) {
            element.bind("mouseenter", function() {
                console.log(element.html() + " has " + scope.abilities);
            });
        }
    };
});

// Ability Attribute directives that communicate with the Superhero Element Directive
app.directive('strength', function(){
    return {
        restrict: 'A',
        require: "superhero",
        link: function (scope, element, attrs, superheroCtrl) {
            superheroCtrl.addStrength();
        }
    };
});

app.directive('speed', function(){
    return {
        restrict: 'A',
        require: "superhero",
        link: function (scope, element, attrs, superheroCtrl) {
            superheroCtrl.addSpeed();
        }
    };
});

app.directive('flight', function(){
    return {
        restrict: 'A',
        require: "superhero",
        link: function (scope, element, attrs, superheroCtrl) {
            superheroCtrl.addFlight();
        }
    };
});

//Directive and Controller communication
//*******************************************************************
app.controller('TweetCtrl', ['$scope', function($scope){
    $scope.loadMoreTweets = function() {
        console.log("Loading Tweets...");
    };

    $scope.deleteTweets = function() {
        console.log("Deleting Tweets...");
    };
}]);


app.directive('mouseinto', function(){
    return function(scope, element, attrs) {
        element.bind("mouseenter", function() {
            scope.$apply(attrs.mouseinto);
        }).bind("mouseleave", function() {
            scope.$apply(attrs.mouseout);
        });
    };
});


//Attribute Directive With Behavior attached
//*******************************************************************
app.directive('enter', function(){
    //Shorthand way of creating an attribute directive
    return function (scope, element, attrs) {
        element.bind("mouseenter", function() {
            element.addClass(attrs.enter);
            element.html(attrs.entermessage);
        });
    };
});

app.directive('leave', function(){
    //Shorthand way of creating an attribute directive
    return function (scope, element, attrs) {
        element.bind("mouseleave", function() {
            element.removeClass(attrs.enter);
            element.html(attrs.leavemessage);
        });
    };
});

//Basic Element Directive
//*******************************************************************
app.directive('batman', function(){
    return {
        restrict: 'A',
        link: function () {
            console.log("I'm batman");
        }
    };
});

//Basic Element Directive
app.directive('superman', function(){
    return {
        restrict: 'E',
        template: '<div class="alert alert-info">Here I am to save the day!</div>'
    };
});